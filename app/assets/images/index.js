const images = {
    vi: require('./languages/vietnam.png'),
    en: require('./languages/united-kingdom.png'),
    ko: require('./languages/south-korea.png'),
    china: require('./languages/china.png'),
    france: require('./languages/france.png'),
    indonesia: require('./languages/indonesia.png'),
    japan: require('./languages/japan.png'),
    russia: require('./languages/russia.png'),
    spain: require('./languages/spain.png'),
    thailand: require('./languages/thailand.png'),
};

export default images;