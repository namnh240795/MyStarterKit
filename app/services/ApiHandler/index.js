import apisauce from 'apisauce';

const DEFAULT_TIMEOUT = 30000;
export default class ApiHandler {
    constructor(configs) {
        const apisauce_configs = {
            baseURL: configs.END_POINT,
            timeout: DEFAULT_TIMEOUT,
            headers: {
                'Content-Type': 'application/json',
            }
        };
        if (configs.KEY_NAME) {
            apisauce_configs.params = {
                [configs.KEY_NAME]: configs.KEY_VALUE
            }
        }
        this._instance = apisauce.create(apisauce_configs);
    }

     //handling error
     _handleError = (response) => {
        // server error
        // if (!response.ok && response.status >= 500) {
        //     return Promise.reject({ ...error.server.getDefaultMessage(response), error: true });
        // }
        
        // if (!response.ok && response.status >= 400 && response.status < 500) {
        //     return Promise.reject({ ...error.client.getErrorMessage(response), error: true });
        // }

        // if (!response.ok && response.problem === TIMEOUT_ERROR) {
        //     return Promise.reject({ ...error.network.getTimeoutMessage(), error: true });
        // }

        // if (!response.ok && !response.data) {
        //     return Promise.reject({ ...error.network.getDefaultMessage(), error: true });
        // }
        
        // return Promise.reject({ message: 'UNKNOWN_ERROR', error: true });
    }

    //handling response
    _handleResponse = (response) => {
        // // response not ok so reject and it will go to catch function in promise
        // if (!response.ok) {
        //     return Promise.reject({ ...response, error: true });
        // }

        // if (response.data === 'timeout') {
        //     return Promise.reject({ error: true, problem: TIMEOUT_ERROR });
        // }
        // // response data
        // return response.data;
    }


    // get method
    get = (url, params, configs = null) => {
        return this._instance
            .get(url, params, configs)
            .then(this._handleResponse)
            .catch(this._handleError);
    }

    // create method
    post = (url, body, configs) => {
        return this._instance
            .post(url, body, configs)
            .then(this._handleResponse)
            .catch(this._handleError);
    }


    // update method
    put = (url, body, configs) => {
        return this._instance
            .put(url, body, configs)
            .then(this._handleResponse)
            .catch(this._handleError);
    }

    // delete method
    delete = (url, params, configs) => {
        return this._instance
            .delete(url, params, configs)
            .then(this._handleResponse)
            .catch(this._handleError);
    }
}