import { takeEvery } from "@redux-saga/core/effects";
import { doExample } from "redux-sagas/sagas/user/doExample";
import { exampleSaga } from "redux-sagas/types";

export default function* userWatcher() {
    yield takeEvery(exampleSaga, doExample);
}