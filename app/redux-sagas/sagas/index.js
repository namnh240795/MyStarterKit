import { all } from "@redux-saga/core/effects";
import userWatcher from "redux-sagas/sagas/user";

export default function* rootSaga() {
    yield all([
        userWatcher()
    ])
}