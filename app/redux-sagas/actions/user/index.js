import { exampleSaga } from "redux-sagas/types";

export const doExample = args => ({ 
    type: exampleSaga,
    ...args
});