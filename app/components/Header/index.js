/* eslint-disable no-nested-ternary */
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { getStyle } from 'themes';

@connect(state => ({

}), {

})
export default class Header extends PureComponent {

    render() {
        return (
            <View 
                style={[
                    getStyle('headerContainer'),
                    this.props.style
                ]}
            >
                {this.props.children}
            </View>
        );
    }
}