import React from 'react';
import { 
    createBottomTabNavigator,
    createAppContainer,
    createStackNavigator,
    createDrawerNavigator
} from 'react-navigation';
import { COLORS } from 'themes/theme-variables';

import Icon from 'components/Icon';
import Home from 'containers/Home';
import Notification from 'containers/Notification';
import Menu from 'containers/Menu';
import Voucher from 'containers/Voucher';
import ChangeLanguage from 'containers/Menu/components/ChangeLanguage';

const getTabIconName = (routeName) => {
    switch (routeName) {
        case 'Home':
            return 'home';
        case 'Menu':
            return 'bars';
        case 'Notification':
            return 'bell';
        case 'Voucher':
            return 'coupon';
        default:
            return;
    }
}

const TabNavigator = createBottomTabNavigator({
    Home,
    Notification,
    Voucher,
    Menu: createStackNavigator({
        Menu,
        ChangeLanguage
    }),
}, {
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            const iconName = getTabIconName(routeName);
            return (
                <Icon 
                    name={iconName}
                    size={focused ? 28 : 24 }
                    color={tintColor}
                />
            )
        },
    }),
    tabBarOptions: {
        activeTintColor: COLORS.red,
        inactiveTintColor: COLORS.grey2,
        showLabel: false
    },
});

const DrawerNavigator = createDrawerNavigator({
    TabNavigator
});


export default createAppContainer(DrawerNavigator);