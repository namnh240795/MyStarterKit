import { Dimensions, StyleSheet, Platform } from 'react-native';

export const DEVICE_WIDTH = Dimensions.get('window').width;
export const DEVICE_HEIGHT = Dimensions.get('window').height;
import { getStatusBarHeight } from 'react-native-status-bar-height';

export const SIZE = [40, 80, 120];

export const COLORS = {
    lightGreen: '#37C892',
    red: '#f44336',
    grey: '#666666',
    grey2: '#999999',
    white: '#ffffff'
};

export const TEXT = {
    i: {
        fontStyle: 'italic'
    },
    'r-rg': {
        fontFamily: 'Roboto-Regular'
    },
    'r-m': {
        fontFamily: 'Roboto-Medium'
    },
    'r-b': {
        fontFamily: 'Roboto-Bold'
    }
};

export const FLEX_BOX = {
    absolute: {
        position: 'absolute'
    },
    'flex-1': {
        flex: 1
    },
    grow: {
        flexGrow: 1
    },
    row: {
        flexDirection: 'row'
    },
    'row-r': {
        flexDirection: 'row-reverse'
    },
    'col-r': {
        flexDirection: 'column-reverse'
    },
    'f-wrap': {
        flexWrap: 'wrap'
    },
    'aifs': {
        alignItems: 'flex-start'
    },
    'aic': {
        alignItems: 'center'
    },
    'aife': {
        alignItems: 'flex-end'
    },
    'asfs': {
        alignSelf: 'flex-start'
    },
    'asc': {
        alignSelf: 'center'
    },
    'asfe': {
        alignSelf: 'flex-end'
    },
    'ass': {
        alignSelf: 'stretch'
    },
    'jcfs': {
        justifyContent: 'flex-start'
    },
    'jcfe': {
        justifyContent: 'flex-end'
    },
    'jcc': {
        justifyContent: 'center'
    },
    'jcsb': {
        justifyContent: 'space-between'
    },
    'jcsa': {
        justifyContent: 'space-around'
    }
};

export const VIEW = {
    'ph-8': {
        paddingHorizontal: 8,
    },
    'ph-16': {
        paddingHorizontal: 16,
    },
    'pv-8': {
        paddingVertical: 8
    },
    'mt-32': {
        marginTop: 32
    },
    'headerContainer': {
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: StyleSheet.hairlineWidth,
        paddingHorizontal: 10,
        backgroundColor: COLORS.white,
        paddingTop: getStatusBarHeight(),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: Platform.select({
            android: 56,
            default: 44,
        }) + getStatusBarHeight(),
    }
};