import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native'; 
import { getStyle } from 'themes';
import i18next from 'i18next';
import { connect } from 'react-redux';
import { doExample } from 'redux-sagas/actions/user';
import Header from 'components/Header';

@connect(state => ({}), {
    doExample
})
export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.re_render = this.props.navigation.addListener('willFocus', () => {
            this.forceUpdate();
        });
    }

    componentWillUnmount() {
        this.re_render;
    }


    render() {
        return (
            <View style={getStyle('flex-1')}>
                <Header>
                    
                </Header>
                <Text style={getStyle('r-rg')}>{i18next.t('HI')}</Text>
                <Text style={getStyle('r-b')}>{i18next.t('HI')}</Text>
                <Text style={getStyle('r-m')}>{i18next.t('HI')}</Text>
                <TouchableOpacity
                    onPress={() => {
                        this.props.doExample({ a: 'b', c: 1 });
                    }}
                >
                    <Text>Press me example saga</Text>
                </TouchableOpacity>
            </View>
        )
    }
}