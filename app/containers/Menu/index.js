import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native'; 
import { getStyle } from 'themes';
import i18next from 'i18next';
import { connect } from 'react-redux';
import { changeLanguage } from 'redux/actions/language';
import NotLoggedIn from 'containers/Menu/components/NotLoggedIn';

const mapStateToProps = state => ({
    language: state.languageReducer.language,
    token: state.userReducer.token,
});

const mapActionToProps = state => ({

});

@connect(mapStateToProps, mapActionToProps)
export default class Menu extends React.Component {
    static navigationOptions = { header: null };

    constructor(props) {
        super(props);
       
    }

    componentDidMount() {
        this.re_render = this.props.navigation.addListener('willFocus', () => {
            
            this.forceUpdate();
        });
    }

    componentWillUnmount() {
        this.re_render;
    }

    onPress = () => {
        this.props.navigation.navigate('ChangeLanguage');
    }

    render() {
        const { token, language } = this.props;
        
        if (!token) {
            return <NotLoggedIn navigation={this.props.navigation} />;
        }
        return <LoggedIn />
        // return (
        //     <View
        //         style={getStyle('flex-1 jcc')}
        //     >
        //         <TouchableOpacity
        //             onPress={this.onPress}
        //         >
        //             <Text>{i18next.t('HI')}</Text>
        //         </TouchableOpacity>
        //     </View>    
        // )
    }
}