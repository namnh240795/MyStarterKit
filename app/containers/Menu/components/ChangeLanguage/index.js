import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { changeLanguage } from 'redux/actions/language';
import images from 'assets/images';
import { getStyle } from 'themes';

const mapStateToProps = state => ({
    
});

const mapActionToProps = {
    changeLanguage
};

@connect(null, {
    changeLanguage
})
export default class ChangeLanguage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            languages: [
                {
                    lng: 'en',
                    name: 'English',
                },
                {
                    lng: 'vi',
                    name: 'Tiếng Việt'
                },
                {
                    lng: 'ko',
                    name: '한국어'
                }
            ]
        }
    }

    switchLanguage = (lng) => {
        i18next.changeLanguage(lng);
        this.props.changeLanguage(lng);
        this.props.navigation.goBack();
    }

    render() {
        const { languages } = this.state;

        return (
            <View>
                {
                    languages.map(l =>
                        <TouchableOpacity
                            onPress={() => this.switchLanguage(l.lng)}
                            key={l.lng}
                            style={getStyle('row pv-8 ph-8')}
                        >
                            <Image source={images[l.lng]} />
                            <Text
                                style={getStyle('ph-8')}
                            >{l.name}</Text>
                        </TouchableOpacity>
                    )
                }
            </View>
        );
    }
}