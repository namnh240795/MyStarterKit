import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import CurrentLanguage from 'containers/Menu/components/CurrentLanguage';
import { getStyle } from 'themes';

const mapStateToProps = state => ({
    language: state.languageReducer.language
});

const mapActionToProps = state => ({
    
});

@connect(mapStateToProps, mapActionToProps)
export default class NotLoggedIn extends React.Component {

    render() {
        return (
            <View style={getStyle('mt-32 pv-8')}>
                <CurrentLanguage navigation={this.props.navigation} />
            </View>
        );
    }
}
