import React, { PureComponent } from 'react';
import Icon from 'components/Icon';
import { Image, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import i18next from 'i18next';
import { getStyle } from 'themes';
import images from 'assets/images';
import { COLORS } from 'themes/theme-variables';

const mapStateToProps = state => ({
    language: state.languageReducer.language
});

@connect(mapStateToProps, null)
export default class CurrentLanguage extends React.Component {
    goChangeLanguage = () => {
        this.props.navigation.navigate('ChangeLanguage');
    }

    render() {
        const { translations, language } = this.props;
        return (
            <TouchableOpacity
                onPress={this.goChangeLanguage}
                style={getStyle('ph-16 row aic')}
            >
                <Image source={images[this.props.language]}/>
                <Text style={getStyle('ph-8')}>{i18next.t('HI')}</Text>
                <Icon name='angle-down' color={COLORS.red} />
            </TouchableOpacity>
        );
    }
}
