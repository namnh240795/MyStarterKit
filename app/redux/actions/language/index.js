import { CHANGE_LANGUAGE } from "redux/types";

export const changeLanguage = payload => ({
    type: CHANGE_LANGUAGE,
    payload
});