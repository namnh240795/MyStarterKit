import { SAVE_USER_INFO } from "redux/types";
import { REHYDRATE } from "redux-persist";

const initialState = {
    token: null
};

const userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SAVE_USER_INFO:
            return { ...state, ...payload };
        case REHYDRATE:
            console.log('rehydrate', payload);
            return { ...state };
        default:
            return { ...state };
    }
};

export default userReducer;