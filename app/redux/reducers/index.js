import { combineReducers } from "redux";
import languageReducer from "redux/reducers/language";
import userReducer from "redux/reducers/user";

const appReducer = combineReducers({
    languageReducer,
    userReducer
});

export default appReducer;