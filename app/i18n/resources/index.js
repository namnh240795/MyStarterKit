const resources = {
    en: {
        translation: require('./en/translation.json')
    },
    ko: {
        translation: require('./ko/translation.json'),
    },
    vi: {
        translation: require('./vi/translation.json')
    }
};

export default resources;