unzip -o public/fonts/Roboto.zip -d public/fonts
unzip -o public/Icomoon/icomoon.zip -d public/Icomoon

cp public/fonts/Roboto-Regular.ttf app/assets/fonts
cp public/fonts/Roboto-Medium.ttf app/assets/fonts
cp public/fonts/Roboto-Bold.ttf app/assets/fonts

cp public/fonts/Roboto-Regular.ttf android/app/src/main/assets/fonts
cp public/fonts/Roboto-Medium.ttf android/app/src/main/assets/fonts
cp public/fonts/Roboto-Bold.ttf android/app/src/main/assets/fonts

cp public/Icomoon/fonts/icomoon.ttf app/assets/fonts
cp public/Icomoon/fonts/icomoon.ttf android/app/src/main/assets/fonts
cp public/Icomoon/selection.json app/components/Icon
