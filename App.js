import React, { Component } from 'react';
import ABC from 'routes/index';
import resources from 'i18n/resources';
import { buildTheme } from 'themes';
import i18next from 'i18next';
import appReducer from 'redux/reducers';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'redux-sagas/sagas';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, appReducer)
const sagaMiddleware = createSagaMiddleware();
const middleWares = [sagaMiddleware];

const store = createStore(
    persistedReducer,
    {},
    compose(
        applyMiddleware(sagaMiddleware), 
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ),
);

const persistOptions = {};

const persistedStore = persistStore(store, {}, () => {
    console.log('ready when persist finish');
});

sagaMiddleware.run(rootSaga);

store.subscribe(() => console.log(store.getState()))

buildTheme();
// i18next.init({ resources, lng: 'en', fallbackLng: ['en']}, (err, t) => {});

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Provider store={store}>
                <ABC
                    ref={ref => this.navigator = ref}
                />
            </Provider>
        );
    }
}
